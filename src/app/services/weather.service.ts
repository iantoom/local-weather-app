import { ICurrentWeather } from './../model/interfaces';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface ICurrentWeatherData {
  weather: [{
    description: string,
    icon: string
  }];
  main: {
    temp: number
  };
  sys: {
    country: string
  };
  dt: number;
  name: string;
}

/* We need to ensure that APIs for the actual implementation
and the test double don't go out of sync over time. We can
accomplish this by creating an interface for the service. */
export interface IWeatherService {
  getCurrentWeather(city: string, country: string): Observable<ICurrentWeather>;
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService implements IWeatherService {

  constructor(private http: HttpClient) { }

  getCurrentWeather(search: string | number, country?: string): Observable<ICurrentWeather> {
      let uriParams = '';
      if (typeof search === 'string') {
        uriParams = `q=${search}`;
      } else {
        uriParams = `zip=${search}`;
      }

      if (country) {
        uriParams = `${uriParams},${country}`;
      }

      return this.getCurrentWeatherHelper(uriParams);
  }

  private getCurrentWeatherHelper (uriParams: string) {
    return this.http.get<ICurrentWeatherData>(

      `${environment.baseUrl}api.openweathermap.org/data/2.5/weather?` +
      `${uriParams}&appid=${environment.appId}`

    ).pipe(map(data =>
        this.ICurrentWeatherDataToICurrenrWeather(data)));
  }

  getCurrentWeatherByCoords(coords: Coordinates): Observable<ICurrentWeather> {
    const uriParams = `lat=${coords.latitude}&lon=${coords.longitude}`;
    return this.getCurrentWeatherHelper(uriParams);
  }

  private ICurrentWeatherDataToICurrenrWeather(data: ICurrentWeatherData): ICurrentWeather {
    return {
      city: data.name,
      country: data.sys.country,
      date: data.dt * 1000,
      image: `http://openweathermap.org/img/w/${data.weather[0].icon}.png`,
      temperature: this.convertKelvinToCelcius(data.main.temp),
      description: data.weather[0].description
    };
  }

  private convertKelvinToCelcius(kelvin: number): number {
    return kelvin - 273.15;
  }
}
