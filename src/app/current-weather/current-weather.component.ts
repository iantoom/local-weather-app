import { WeatherService } from './../services/weather.service';
import { ICurrentWeather } from './../model/interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {

  current: ICurrentWeather;

  constructor(private weatherService: WeatherService) {
    // Initialize default variable value in constructor
    // btw, this is unnecessary if you use class instead of interface
    this.current = {
      city: '',
      country: '',
      date: 0,
      image: '',
      temperature: 0,
      description: '',
    };
   }

  ngOnInit() {
    this.weatherService.getCurrentWeather('Cirebon', 'ID')
      .subscribe(data => this.current = data);
  }

}
